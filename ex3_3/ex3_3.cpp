#include <stdlib.h>
#include <iostream>
#include <utility>
#include <stack>

/**
 * @class Treap
 * @brief Декартово дерево
 * */
template<typename key_t, 
         typename prior_t>
struct Treap {
  typedef bool (*less_key_t)(key_t&, key_t&);
  typedef bool (*less_prior_t)(prior_t&, prior_t&);

  key_t x;
  prior_t y;

  ///Функция сравнения ключей
  static less_key_t less_key;
  ///Функция сравнения значений
  static less_prior_t less_prior;

  Treap *left, *right;

  Treap(key_t _x,
        prior_t _y,
        Treap *_left = NULL,
        Treap *_right = NULL):
  x(_x),
  y(_y),
  left(_left),
  right(_right) {}

  ~Treap() {
    if (left) delete left;
    if (right) delete right;
  }

  Treap(key_t _x,
        prior_t _y,
        less_key_t _lk,
        less_prior_t _lp,
        Treap *_left = NULL,
        Treap *_right = NULL):
  x(_x),
  y(_y),
  left(_left),
  right(_right) {
     less_key = _lk;
     less_prior = _lp;
   }

  prior_t& priority() {return y;}
  key_t& key() {return x;}

  static Treap *merge(Treap *l, Treap *r) {
    Treap *t;
    if (!l) return r;
    if (!r) return l;
    if (l->less_key(r->key(), l->key())) {
      t = l;
      t->right = Treap::merge(l->right, r);
    } else {
      t = r;
      t->left = Treap::merge(l, r->left);
    }
    return t;
  }

  static std::pair<Treap*, Treap*>
    split(Treap *root, key_t key) {
      std::pair<Treap*, Treap*> result, buf; 
      if (!root) {
        result.first = NULL;
        result.second = NULL;
      } else {
        if (root->less_key(root->key(), key)) {
          result.first = root;
          buf = split(root->right, key);
          root->right = buf.first;
          result.second = buf.second;
        } else {
          result.second = root;
          buf = split(root->left, key);
          root->left = buf.second;
          result.first = buf.first;
        }
        return result;
      }
      return result;
  }

  static void add(Treap *&tr, key_t _key, prior_t _pr) {
    std::pair<Treap<key_t, prior_t>*,
              Treap<key_t, prior_t>*> buf;
    if (tr == NULL) {
      tr = new Treap(_key, _pr); 
      return;
    }
    if (less_prior(tr->priority(), _pr)) {
      buf = split(tr, _key);
      tr = new Treap(_key, _pr, buf.first, buf.second); 
    } else {
      if (less_key(tr->key(), _key))
        add(tr->right, _key, _pr);
      else
        add(tr->left, _key, _pr);
    }
  }

  unsigned depth() {
    unsigned d = 1, rd = 0, ld = 0;
    if (left) ld = left->depth();
    if (right) rd = right->depth();
    return d + std::max(ld, rd);
  }
};

template<typename T, typename J>
typename Treap<T,J>::less_key_t Treap<T,J>::less_key;

template<typename T, typename J>
typename Treap<T,J>::less_prior_t Treap<T,J>::less_prior;

template<typename T>
bool st_less(T val1, T val2) {
  return val1 < val2;
}

/**
 * @class node
 * @brief Бинарное дерево с наивным порядком вставки из задачи 2.
 * */
template<typename T>
struct node {
  T value;
  node<T> *left, *right;
  
  node<T>(T _value = 0, node<T> *_left = NULL, node<T> *_right = NULL)
    :value(_value),
     left(_left),
     right(_right)
  {}
  
  void insert(T _value) {
    node<T> *current = this;
    bool inserted = false;
    while (!inserted) {
      if (_value < current->value) {
        if (current->left) {
          current = current->left;
        } else {
          current->left = new node<T>(_value);
          inserted = true;
        }
      } else {
        if (current->right) {
          current = current->right;
        } else {
          current->right = new node<T>(_value);
          inserted = true;
        }
      }
    }
  }
  
  void clear() {
    std::stack<node<T>*> st;
    node<T> *buf, *tree = this;
	  while (!st.empty() || tree) {
			if (tree) {
				st.push(tree);
				tree = tree->left;
			} else {
				tree = st.top();
				st.pop();
				buf = tree->right;
				if (tree != this)
					delete tree;
				tree = buf;
			}
		}
  }
  
  unsigned depth() {
    unsigned d = 1, rd = 0, ld = 0;
    if (left) ld = left->depth();
    if (right) rd = right->depth();
    return d + std::max(ld, rd);
  }
};

int main(void) {
  long val, pri;
  unsigned num, depth1, depth2;
  Treap<long, long> *treap = NULL;
  node<long> tree;
  std::cin >> num;
  while(num--) {
    std::cin >> val;
    std::cin >> pri;
    if (treap) {
      Treap<long, long>::add(treap, val, pri);
      tree.insert(val);
    } else {
      treap = new Treap<long, long>(val, pri, st_less, st_less);
      tree.value = val;
    }
  }
  depth1 = tree.depth();
  depth2 = treap->depth();
  std::cout << (depth1 - depth2);
  delete treap;
  tree.clear();
  return 0;
}
