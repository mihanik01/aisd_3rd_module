#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Variant 2
//(double hash)

class hash_table{
  public:
    hash_table(unsigned init_size = 8);
    bool add(const char *value);
    char* find(const char *string);
    bool remove(const char *string);
    ~hash_table();
  private:
    ///Максимальный коэффициент заполнения
    const float alpha = 3./4;
    const void* DELETED = (void*) (0x01);
    unsigned size, inserted;
    char **data;
    bool add_cpy(char *value);
    unsigned hash1(const char* value);
    unsigned hash2(const char* value);
    void re_hash();
};

hash_table::hash_table(unsigned init_size):
  size(init_size), inserted(0){
  data = new char* [size];
  for(unsigned i = 0; i < size; i++)
    data[i] = NULL;
}

hash_table::~hash_table(){
  for (unsigned i = 0; i < size; i++)
	if(data[i] && data[i] != DELETED)
	  delete[] data[i];
  delete[] data;
}

unsigned hash_table::hash1(const char *k) {
  const unsigned M = 65011; //Простое число
  const unsigned a = 36217; //Тоже простое число
  unsigned long sum = 0;
  for (; *k; k++) {
    sum += (unsigned char) *k;
    sum *= a;
  }
  return sum % M;
}

unsigned hash_table::hash2(const char *k) {
  const unsigned M = 62039; //Простое число
  const unsigned a = 17489; //Тоже простое число
  unsigned long sum = 0;
  for (; *k; k++) {
    sum += (unsigned char) *k;
    sum *= a;
  }
  return (sum % M)*2+1;
}

bool hash_table::add(const char *value){
  char* buf = new char[strlen(value) + 1];
  if (!find(value)) {
    strcpy(buf, value);
    add_cpy(buf);
    ++inserted;
    return true;
  }
  return false;
}

bool hash_table::add_cpy(char *value){
  unsigned key;
  for(unsigned i = 0; i < size; i++) {
    key = (hash1(value) + i * hash2(value)) % size;
    if(data[key] == NULL || data[key] == DELETED) {
      data[key] = value;
      if((float) inserted / size >= alpha)
        re_hash();
      return true;
    }
  }
  return false;
}

char* hash_table::find(const char *string){
  unsigned key;
  for(unsigned i = 0; i < size; i++) {
    key = (hash1(string) + i * hash2(string)) % size;
    if(data[key] == NULL)
      return NULL;
    if(data[key] != DELETED && !strcmp(string, data[key]))
      return data[key];
  }
  return NULL; 
}


bool hash_table::remove(const char* string) {
  unsigned key;
  for(unsigned i = 0; i < size; i++) {
    key = (hash1(string) + i * hash2(string)) % size;
    if(data[key] == NULL)
      return false;
    if(data[key] != DELETED && !strcmp(string, data[key])) {
      delete[] data[key];
      data[key] = (char*) DELETED;
      --inserted;
      return true;
    }
  }
  return false;
}

void hash_table::re_hash(){
  char** old_data = data;
  unsigned old_size = size;
  
  size <<= 1;
  data = new char* [size];
  for (unsigned i = 0; i < old_size; i++) {
    if (old_data[i] && old_data[i] != DELETED)
      add_cpy(old_data[i]);
  }
  delete[] old_data;
}

int main(void){
  hash_table tbl;
  char string[256], operation;
  bool result;
  while (scanf("%c %s", &operation, string) != EOF) {
	getchar();
    switch (operation) {
      case '+':
        result = tbl.add(string);
        break;
      case '-':
        result = tbl.remove(string);
        break;
      case '?':
        result = tbl.find(string);
        break;
    }
    if (result)
      printf("OK\n");
    else
      printf("FAIL\n");
  }
  return 0;
}
