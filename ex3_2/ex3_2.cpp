#include <stdlib.h>
#include <iostream>
#include <stack>
#include <stdio.h>

template<typename T>
struct node {
  T value;
  node<T> *left, *right;
  
  node<T>(T _value = 0, node<T> *_left = NULL, node<T> *_right = NULL)
    :value(_value),
     left(_left),
     right(_right)
  {}
  
  void insert(T _value) {
    node<T> *current = this;
    bool inserted = false;
    while (!inserted) {
      if (_value < current->value) {
        if (current->left) {
          current = current->left;
        } else {
          current->left = new node<T>(_value);
          inserted = true;
        }
      } else {
        if (current->right) {
          current = current->right;
        } else {
          current->right = new node<T>(_value);
          inserted = true;
        }
      }
    }
  }
  
  void clear() {
    std::stack<node<T>*> st;
    node<T> *buf, *tree = this;
	  while (!st.empty() || tree) {
			if (tree) {
				st.push(tree);
				tree = tree->left;
			} else {
				tree = st.top();
				st.pop();
				buf = tree->right;
				if (tree != this)
					delete tree;
				tree = buf;
			}
		}
  }
  
};

template<typename action_t, typename T>
void act_in_order(node<T> *tree, action_t &action) {
  std::stack<node<T>*> st;
  while (!st.empty() || tree) {
		if (tree) {
			st.push(tree);
			tree = tree->left;
		} else {
			tree = st.top();
			st.pop();
			action(tree);
			tree = tree->right;
		}
	}
}

template<typename T>
void print(node<T> *n) {
  std::cout << n->value << " ";
}

int main(void) {
  unsigned long N;
  long value;
  node<long> tree;
  std::cin >> N;
  if (N--) {
    std::cin >> value;
    tree.value = value;
    while (N--) {
      std::cin >> value;
      tree.insert(value);
    }
  }
  act_in_order(&tree, print<long>);
  tree.clear();
  return 0;
}
